<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_user">
		        <span>Peter hoffmaN</span>
	        </div>

	        <div class="account">
		        <div class="account_side">
			        <div class="account_side_title">Benutzer</div>

			        <ul class="account_nav">
				        <li class="active">
					        <a href="#">
						        <span>Мои настройки</span>
						        <i class="account_nav_icon icon_setting">
							        <svg class="ico-svg"  viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg">
								        <use xlink:href="build/images/sprite_icons.svg#icon_setting" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							        </svg>
						        </i>
					        </a>
				        </li>
				        <li>
					        <a href="#">
						        <span>выход</span>
						        <i class="account_nav_icon icon_logout">
							        <svg class="ico-svg" viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
								        <use xlink:href="build/images/sprite_icons.svg#icon_logout" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							        </svg>
						        </i>
					        </a>
				        </li>
			        </ul>

		        </div>
		        <div class="account_body">

			        <div class="account_title">Мои настройки</div>

			        <div class="account_content">

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>Пользователь</h4>
					        </div>
					        <div class="account_box_content">
						        <div class="row">
							        <div class="col-xs-12 col-md-6">
								        <div class="form_group">
									        <label class="form_label">Имя, фамилия</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control" value="Peter hoffmaN">
									        </div>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>Мой e-mail</h4>
					        </div>
					        <div class="account_box_content">
						        <div class="row">
							        <div class="col-xs-12 col-md-6">
								        <div class="form_group">
									        <label class="form_label">Мой e-mail</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control" value="4654684@gmail.com">
									        </div>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>Смена пароля</h4>
					        </div>
					        <div class="account_box_content">

						        <div class="row">
							        <div class="col-xs-12 col-md-6">
								        <div class="form_group">
									        <label class="form_label">старый пароль</label>
									        <div class="form_item">
										        <input type="password" name="" class="form_control" value="" placeholder="***********">
									        </div>
								        </div>
							        </div>
						        </div>

						        <div class="account_box_gray">
							        <div class="row">
								        <div class="col-xs-12 col-md-6">
									        <div class="form_group">
										        <label class="form_label">Новый пароль</label>
										        <div class="form_item">
											        <input type="password" name="" class="form_control" value="" placeholder="***********">
										        </div>
									        </div>
								        </div>
								        <div class="col-xs-12 col-md-6">
									        <div class="form_group">
										        <label class="form_label">Повторите новый пароль</label>
										        <div class="form_item">
											        <input type="password" name="" class="form_control" value="" placeholder="***********">
										        </div>
									        </div>
								        </div>
							        </div>
							        <br/>
							        <button type="submit" class="btn_main btn_md">Установить новый пароль</button>
						        </div>

					        </div>
				        </div>

			        </div>
		        </div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

            <div class="provider">
                <div class="provider_title">Anbieter</div>
                <ul class="provider_row">
                   <li>
                       <label>
                           <input type="radio" name="provider" value="1" checked>
                           <div class="provider_item">
                               <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                               <span>Online</span>
                           </div>
                       </label>
                   </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                </ul>
            </div>

	        <ul class="contract_progress">
		        <li><span>1. Создание договора</span></li>
		        <li><span>2. Подписание договора</span></li>
		        <li class="active"><span>3. Краткая информация</span></li>
	        </ul>

	        <div class="contract_header">Informationen zum Vertrag</div>
	        <div class="contract">

		        <div class="contract_content">

			        <div class="contract_user">
				        <div class="contract_user_name">
					        <div class="contract_user_wrap">jHON SNOW</div>
				        </div>
				        <div class="contract_user_info">
					        <div class="contract_user_wrap">
						        <span>Vertragsnummer</span>
						        <strong>EPOS02014905975193</strong>
					        </div>
				        </div>
			        </div>

			        <div class="contract_data">

				        <div class="form_block">
					        <div class="form_block_title">dokument</div>
					        <div class="form_block_content">
						        <div class="row">
							        <div class="col-md-6">
								        <div class="form_group">
									        <label class="form_label">Staatsangehorigkeit</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control" placeholder="Deutschland">
									        </div>
								        </div>
							        </div>
							        <div class="col-md-6">
								        <div class="form_group">
									        <label class="form_label">Ausweisnummer</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control" placeholder="MR38918498183TW" value="">
									        </div>
								        </div>
							        </div>
							        <div class="col-md-6">
								        <div class="form_group">
									        <label class="form_label">Gultig bis</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control" placeholder="03/05/1985">
									        </div>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="form_block">
					        <div class="form_block_title">IMEI</div>
					        <div class="form_block_content">
						        <div class="row">
							        <div class="col-md-6">
								        <div class="form_group">
									        <label class="form_label">imei hw-band-tarifaupreis</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control" placeholder="123456789012">
									        </div>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="form_block">
					        <div class="form_block_title">sIM</div>
					        <div class="form_block_content">
						        <div class="row">
							        <div class="col-md-6">
								        <div class="form_group">
									        <label class="form_label">simkarten nummer</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control" placeholder="123456789012">
									        </div>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="form_block">
					        <div class="form_block_title">tARIF PLAN</div>
					        <div class="form_block_content">
						        <ul class="tariff_summary">
							        <li>
								        <div class="tariff_summary_title"><span>Tarif</span></div>
								        <div class="tariff_summary_row">
									        <strong>€29.99</strong>
									        <span>ay allnet plus</span>
								        </div>
							        </li>
							        <li>
								        <div class="tariff_summary_title"><span>Packet</span></div>
								        <div class="tariff_summary_row">
									        <strong>€5.00</strong>
									        <span>hw-band-tarifaupreis</span>
								        </div>
								        <div class="tariff_summary_row">
									        <strong>€0.00</strong>
									        <span>100% Rabatt auf Anschlusspreis-AyYildiz</span>
								        </div>
							        </li>
							        <li>
								        <div class="tariff_summary_title"><span></span></div>
								        <div class="tariff_summary_row">
									        <strong></strong>
									        <span></span>
								        </div>
							        </li>
							        <li>
								        <div class="tariff_summary_title"><span>insgesamt</span></div>
								        <div class="tariff_summary_row">
									        <strong>€0.00</strong>
									        <span>Einmaling</span>
								        </div>
							        </li>
						        </ul>
					        </div>
				        </div>

			        </div>

		        </div>

		        <aside class="contract_side">
			        <div class="contract_side_container">

				        <div class="contract_side_heading">
					        <span>Statusänderung</span>
				        </div>

				        <div class="contract_side_wrap">
					        <ul class="contract_side_history">
						        <li rel="history_new">
							        <strong>Создан</strong>
							        <span>15.03.2018, 14:21</span>
						        </li>
						        <li rel="history_process">
							        <strong>В обработке</strong>
							        <span>15.03.2018, 14:21</span>
						        </li>
						        <li rel="history_finished">
							        <strong>Завершен</strong>
							        <span>15.03.2018, 14:21</span>
						        </li>
						        <li rel="history_success">
							        <strong>Успешно</strong>
							        <span>15.03.2018, 14:21</span>
						        </li>
					        </ul>
				        </div>
			        </div>
		        </aside>

	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

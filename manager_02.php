<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">
		        </div>
		        <div class="account_heading_right">
			        <h1>создание  нового магазина</h1>
		        </div>
	        </div>

	        <div class="account_top">
		        <div class="account_top_name">настройки нового магазина</div>
		        <div class="account_top_actions"><a href="#">Закрыть магазин</a></div>
	        </div>

	        <div class="content_box">
		        <form class="form">
			        <div class="form_block">
				        <div class="form_block_title">Магазин</div>
				        <div class="form_block_content">
					        <div class="form_group">
						        <label class="form_label">Название магазина</label>
						        <div class="form_item">
							        <input type="text" name="" class="form_control form_control_bold" placeholder="Shop3943">
						        </div>
					        </div>
					        <div class="form_group">
						        <label class="form_label">АДРЕС МАГАЗИНА</label>
						        <div class="form_item">
							        <input type="text" name="" class="form_control form_control_bold" placeholder="Ул. Пшукинка 23 д. 145">
						        </div>
					        </div>
				        </div>
			        </div>

			        <div class="form_block">
				        <div class="form_block_title">КонфигурациИ</div>
				        <div class="form_block_content">
					        <div class="row">
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1" checked>
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1" checked>
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1" checked>
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
					        </div>
				        </div>
			        </div>

			        <div class="form_block">
				        <div class="form_block_title">Вход в магазин</div>
				        <div class="form_block_content">
					        <div class="row">
						        <div class="col-xs-12 col-md-6 col-xl-4">
							        <div class="form_group">
								        <label class="form_label">E-mail</label>
								        <div class="form_item">
									        <input type="text" name="" class="form_control" value="shop-3294-23@simregs.com" placeholder="shop-3294-23@simregs.com">
								        </div>
							        </div>
						        </div>
					        </div>
					        <div class="row">
						        <div class="col-xs-12 col-md-6 col-xl-4">
							        <div class="form_group">
								        <label class="form_label">Password</label>
								        <div class="form_item">
									        <input type="password" name="" class="form_control" placeholder="KJlgj32dsjl4832093">
								        </div>
							        </div>
						        </div>
					        </div>

					        <div class="additional_title">дополнительные аккаунты для входа</div>
					        <div class="additional">
						        <div class="additional_user additional_user_heading">
							        <div class="additional_user_col additional_user_name"><strong>Имя фамилия</strong></div>
							        <div class="additional_user_col additional_user_data"><strong>E-mail</strong></div>
							        <div class="additional_user_col additional_user_remove"></div>
						        </div>
						        <div class="additional_user">
							        <div class="additional_user_col additional_user_name">
								        <div class="form_item">
									        <input class="form_control" type="text" name="" value="ALexei pikarchuk" placeholder="">
								        </div>
							        </div>
							        <div class="additional_user_col additional_user_data">
								        <div class="form_item">
									        <input class="form_control" type="text" name="" value="sdfims298593@simregs.com" placeholder="">
								        </div>
							        </div>
							        <div class="additional_user_col additional_user_remove">
								        <a class="additional_remove" href="#"><span>удалить</span> <i class="fa fa-remove"></i></a>
							        </div>
						        </div>
						        <div class="additional_user">
							        <div class="additional_user_col additional_user_name">
								        <div class="form_item">
									        <input class="form_control" type="text" name="" value="ALexei pikarchuk" placeholder="">
								        </div>
							        </div>
							        <div class="additional_user_col additional_user_data">
								        <div class="form_item">
									        <input class="form_control" type="text" name="" value="sdfims298593@simregs.com" placeholder="">
								        </div>
							        </div>
							        <div class="additional_user_col additional_user_remove">
								        <a class="additional_remove" href="#"><span>удалить</span> <i class="fa fa-remove"></i></a>
							        </div>
						        </div>
						        <div class="additional_user">
							        <div class="additional_user_col additional_user_name">
								        <div class="form_item">
									        <input class="form_control" type="text" name="" value="ALexei pikarchuk" placeholder="">
								        </div>
							        </div>
							        <div class="additional_user_col additional_user_data">
								        <div class="form_item form_ok">
									        <input class="form_control" type="text" name="" value="sdfims298593@simregs.com" placeholder="">
								        </div>
							        </div>
							        <div class="additional_user_col additional_user_remove">
								        <a class="additional_remove" href="#"><span>удалить</span> <i class="fa fa-remove"></i></a>
							        </div>
						        </div>
						        <div class="additional_user additional_user_new">
							        <div class="additional_user_name"></div>
							        <div class="additional_user_data"></div>
							        <div class="additional_user_remove">
								        <a class="additional_new" href="#">Добавить</a>
							        </div>
						        </div>
					        </div>

				        </div>
			        </div>

		        </form>

	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

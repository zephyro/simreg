<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="auth_page">
            <div class="auth">
                <div class="auth_header">
                    <a href="#" class="logo">
                        <img src="build/images/logo.svg" class="img-fluid" alt="">
                        <span class="logo_line"></span>
                    </a>
                </div>
                <div class="auth_row">
                    <div class="auth_left">
                        <div class="auth_image">
                            <img src="build/images/auth_image.svg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="auth_right">
                        <div class="auth_form">
                            <form class="form">
                                <div class="auth_form_title">Passwort Wiederherstellung</div>

                                <div class="form-group">
                                    <div class="form_elem">
                                        <input class="form_elem_control" type="text" name="email" placeholder="Neues Passwort" value="Kappel.Waltraud@gmail.com">
                                        <div class="form_elem_icon icon_envelope">
                                            <svg class="ico-svg"  viewBox="0 0 97 62" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="build/images/sprite_icons.svg#icon_envelope" xmlns: xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="form_elem_info">Geben Sie Ihre E-Mail-Adresse ein, wir senden Ihnen einen Link, um das Passwort wiederherzustellen</div>
                                </div>


                                <div class="auth_form_button">
                                    <button class="btn_large" type="submit">wiederherstellen</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

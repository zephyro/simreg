<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">


    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="main_info">
		        <div class="main_info_title">Meine Informationen</div>
		        <div class="main_info_row">
			        <a class="main_info_item" href="#">
				        <div class="main_info_wrap">
					        <strong>Ожидает подтверждения</strong>
					        <span>5 контрактов</span>
				        </div>
			        </a>
			        <a class="main_info_item" href="#">
				        <div class="main_info_wrap">
					        <strong>Ожидает подтверждения</strong>
					        <span>5 контрактов</span>
				        </div>
			        </a>
			        <a class="main_info_item" href="#">
				        <div class="main_info_wrap">
					        <strong>Зарегистрировано за 7 дней</strong>
					        <span>45 контрактов = 5500 eur</span>
				        </div>
			        </a>
			        <a class="main_info_item" href="#">
				        <div class="main_info_wrap">
					        <strong>Регистраций сим карт сегодня</strong>
					        <span>15 контрактов = 1500eur</span>
				        </div>
			        </a>
		        </div>
	        </div>

	        <div class="dataTable mt_30">
		        <ul class="views">
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="25" checked>
					        <span>25</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="50">
					        <span>50</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="100">
					        <span>100</span>
				        </label>
			        </li>
		        </ul>

		        <div class="table_responsive_md">
			        <div class="table_top"></div>
			        <div class="table_responsive">

				        <table class="table dataTable">

					        <tr class="table_head">
						        <th>
							        <div class="table_head_title">Alle Ladenverträge | 32.432</div>
							        Anbieter
						        </th>
						        <th>Vorname, Name</th>
						        <th>
							        <div class="table_head_date"><span>absteigend</span> | <span class="active">aufsteigend</span></div>
							        <div class="dataTable_sort">
								        <span>создан <i class="fa fa-angle-up"></i></span>
								        <span>обновлен<i class="fa fa-angle-down"></i></span>
							        </div>
						        </th>
						        <th>Vertragsnummer</th>
						        <th>SIM-Nummer Telefonnummer</th>
						        <th>
							        Status
						        </th>
						        <th>hinzugefügt</th>
						        <th>Ansicht</th>
					        </tr>

					        <tr class="table_nav">
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r1" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="T-MOBILE">
												        <input type="radio" name="r1" value="T-MOBILE">
												        <span>T-MOBILE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="BASE">
												        <input type="radio" name="r1" value="BASE">
												        <span>BASE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="O2">
												        <input type="radio" name="r1" value="O2">
												        <span>O2</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="VODAFONE">
												        <input type="radio" name="r1" value="VODAFONE">
												        <span>VODAFONE</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r2" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="DUNKLE LIESE">
												        <input type="radio" name="r2" value="DUNKLE LIESE">
												        <span>DUNKLE LIESE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="ESSER META">
												        <input type="radio" name="r2" value="ESSER META">
												        <span>ESSER META</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="HUMMEL MAYA">
												        <input type="radio" name="r2" value="HUMMEL MAYA">
												        <span>HUMMEL MAYA</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="SCHUSTER BARBARA">
												        <input type="radio" name="r2" value="SCHUSTER BARBARA">
												        <span>SCHUSTER BARBARA</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td></td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r4" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="Нет номера">
												        <input type="radio" name="r4" value="Нет номера">
												        <span>Нет номера</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td></td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r6" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="Черновик">
												        <span class="icon_sort icon_sort_edit">Черновик</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="Ожидание">
												        <span class="icon_sort icon_sort_wait">Ожидание</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="В очереди">
												        <span class="icon_sort icon_sort_turn">В очереди</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="В работе">
												        <span class="icon_sort icon_sort_work">В работе</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="Ошибка">
												        <span class="icon_sort icon_sort_alert">Ошибка</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="Не возможно">
												        <span class="icon_sort icon_sort_invalid">Не возможно</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="Оплачен">
												        <span class="icon_sort icon_sort_paid">Оплачен</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="промодерирован">
												        <span class="icon_sort icon_sort_moderate">промодерирован</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r6" value="Отправлен провайдеру">
												        <span class="icon_sort icon_sort_send">Отправлен провайдеру</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="Заключен с провайдером">
												        <input type="radio" name="r6" value="Заключен с провайдером">
												        <span class="icon_sort icon_sort_order">Заключен с провайдером</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td><div class="table_nav_elem"><span>alles</span><i class="fa fa-angle-down"></i></div></td>
						        <td>скачать csv</td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">
							        <img src="build/images/sim_logo.png" class="img-fluid" alt="">
						        </td>
						        <td class="dataTable_user">Dunkle Liese </td>
						        <td>
							        08.05.2018  11:55<br/>
							        <small>08.05.2018  11:55</small>
						        </td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_turn"></i>
								        <div class="dataTable_status_text">
									        <span>В очереди</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">T-Mobile</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        08.05.2018  11:55
						        </td>
						        <td>нет номера</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_alert"></i>
								        <div class="dataTable_status_text">
									        <span>Отклонен</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">BASE</td>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td>08.05.2018  11:55</td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_work"></i>
								        <div class="dataTable_status_text">
									        <span>В работе</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">O2</td>
						        <td class="dataTable_user">Hummel Maya</td>
						        <td>08.05.2018  11:55</td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_success"></i>
								        <div class="dataTable_status_text">
									        <span>Заключен</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">Vodafone</td>
						        <td class="dataTable_user">pUSHKIN ALEXEY</td>
						        <td>08.05.2018  11:55</td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_edit"></i>
								        <div class="dataTable_status_text">
									        <span>Черновик</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">T-Mobile</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>08.05.2018  11:55</td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_invalid"></i>
								        <div class="dataTable_status_text">
									        <span>Не возможно</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">Vodafone</td>
						        <td class="dataTable_user">pUSHKIN ALEXEY</td>
						        <td>08.05.2018  11:55</td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_paid"></i>
								        <div class="dataTable_status_text">
									        <span>Оплачен</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">T-Mobile</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        08.05.2018  11:55
						        </td>
						        <td>нет номера</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_wait"></i>
								        <div class="dataTable_status_text">
									        <span>На рассмотрении</span>
									        <br/>
									        <a href="#">Проверить сейчас</a>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">T-Mobile</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        08.05.2018  11:55
						        </td>
						        <td>нет номера</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_alert"></i>
								        <div class="dataTable_status_text">
									        <span>Отклонен</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">BASE</td>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td>08.05.2018  11:55</td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_work"></i>
								        <div class="dataTable_status_text">
									        <span>В работе</span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_logo">O2</td>
						        <td class="dataTable_user">Hummel Maya</td>
						        <td>08.05.2018  11:55</td>
						        <td>EPOS02014905975193</td>
						        <td>
							        1234567890123456789<br/>
							        +491724567890
						        </td>
						        <td>
							        <div class="dataTable_status">
								        <i class="icon_status icon_status_success"></i>
								        <div class="dataTable_status_text">
								        <span>Отправлен провайдеру
									        <br/>Заключен с провайдером
									        <br/>Контракт промодерирован
								        </span>
								        </div>
							        </div>
						        </td>
						        <td>
							        <span class="dataTable_icon">ТК</span>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>


				        </table>
			        </div>
		        </div>
				<div class="pagination">
					<ul>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><span>3</span></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">6</a></li>
					</ul>
				</div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">


    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">

		        </div>
		        <div class="account_heading_right">
			        <h1>договор №4828724</h1>

		        </div>
	        </div>


	        <div class="contract_info three_col">
		        <div class="contract_info_title">Информация о договоре</div>
		        <div class="contract_info_box">
			        <div class="contract_info_name">
				        <div class="contract_info_wrap">jHON SNOW</div>
			        </div>
			        <div class="contract_info_text">
				        <div class="contract_info_wrap">
					        F,i: Alex pushkov<br/>
					        дата рождения<br/>
					        Адрес
				        </div>
			        </div>
			        <div class="contract_info_text">
				        <div class="contract_info_wrap">
					        Данные паспорта<br/>
					        Банковские данные<br/>
					        детали контракта
				        </div>
			        </div>

			        <div class="contract_info_data">
				        <a class="contract_info_status" href="#">
					        <strong>Status</strong>
					        <span>Провайдер проверил</span>
				        </a>
			        </div>
		        </div>
	        </div>

	        <div class="content_box">
		        <div class="content_heading">контракт</div>
		        <div class="hr"></div>

		        <ul class="contract_docs">
			        <li class="valid">
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
			        </li>
			        <li class="valid">
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
			        </li>
			        <li class="valid">
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
			        </li>
			        <li class="valid">
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
			        </li>

			        <li>
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
			        <li>
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
			        <li>
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
			        <li>
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
			        <li>
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
			        <li>
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
			        <li class="invalid">
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_invalid disabled">Неверно</a>
				        </div>
			        </li>
			        <li class="invalid">
				        <a class="contract_docs_image" data-fancybox="gallery" href="build/images/contract_image.jpg">
					        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="contract_docs_actions">
					        <a href="#" class="btn_status btn_status_invalid disabled">Неверно</a>
				        </div>
			        </li>
		        </ul>

		        <div class="content_heading">ДОКУМЕНТ ПОДТВЕРЖДАЮЩИЙ ЛИЧНОСТЬ</div>
		        <div class="hr"></div>

		        <ul class="docs_validation">
			        <li>
				        <a data-fancybox="docs" href="build/images/docs_01.jpg" class="docs_validation_image">
					        <img src="build/images/docs_01.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="docs_validation_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
			        <li>
				        <a data-fancybox="docs" href="build/images/docs_02.jpg" class="docs_validation_image">
					        <img src="build/images/docs_02.jpg" class="img-fluid" alt="">
				        </a>
				        <div class="docs_validation_actions">
					        <a href="#" class="btn_status btn_status_valid">Верно</a>
					        <a href="#" class="btn_status btn_status_invalid">Неверно</a>
				        </div>
			        </li>
		        </ul>

		        <div class="block_warning block_alert mb_30">
			        <div class="block_warning_title"><span>важно!</span></div>
			        <div class="block_warning_text">
				        <div class="block_warning_wrap">
					        <strong>Ожидаем договор от провайдера</strong>
					        <br>
					        В ожидании договора
				        </div>
			        </div>
		        </div>

		        <div class="hr"></div>
		        <form class="form">
			        <div class="row">
				        <div class="col-xs-12 col-sm-12 col-lg-4 col-xl-3">
					        <div class="form_title_base">Дополнительный комментарий магазину</div>
				        </div>
				        <div class="col-xs-12 col-sm-8 col-lg-5 col-xl-6">
							<div class="form_group">
								<textarea class="form_control_border" placeholder="Ваш коментарий" rows="6"></textarea>
							</div>
				        </div>
				        <div class="col-xs-12 col-sm-4 col-lg-3 col-xl-3">
					        <button type="submit" class="btn_main btn_md btn_border btn_width">ОДОБРИТЬ</button>
				        </div>
			        </div>
		        </form>

	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

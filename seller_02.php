<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

            <div class="provider">
                <div class="provider_title">Anbieter</div>
                <ul class="provider_row">
                   <li>
                       <label>
                           <input type="radio" name="provider" value="1" checked>
                           <div class="provider_item">
                               <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                               <span>Online</span>
                           </div>
                       </label>
                   </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                </ul>
            </div>

            <ul class="contract_progress">
                <li><span>1. Создание договора</span></li>
                <li class="active"><span>2. Подписание договора</span></li>
                <li><span>3. Краткая информация</span></li>
            </ul>

	        <div class="contract_info">
		        <div class="contract_info_title">Информация о договоре</div>
	            <div class="contract_info_box">
		            <div class="contract_info_name">
			            <div class="contract_info_wrap">jHON SNOW</div>
		            </div>
		            <div class="contract_info_text">
			            <div class="contract_info_wrap">
				            <span>Gultig bis</span>
				            <strong>03/05/1985</strong>
			            </div>
		            </div>
		            <div class="contract_info_data">
			            <a class="contract_info_download" href="#">
				            <span>Download-Vertrag</span>
			            </a>
		            </div>
	           </div>
            </div>

	        <div class="contract">

		        <div class="contract_content">

			        <div class="contract_data">
				        <div class="contract_heading">Загрузите Подписанный контракт</div>

				        <div class="file_block mb_30">
					        <div class="file_block_text">Система автоматического распознавания документов</div>
					        <ul class="file_block_row">
						        <li>
							        <label class="file_upload">
								        <div class="file_upload_wrap">
									        <input type="file" name="" value="">
									        <div class="file_upload_text">Перетащите файлы сюда или кликните для загрузки</div>
								        </div>
							        </label>
						        </li>
						        <li>
							        <div class="qr">
								        <div class="qr_code">
									        <img src="build/images/qr.svg" class="img-fluid" alt="">
								        </div>
							        </div>
						        </li>
					        </ul>
				        </div>

				        <div class="hr"></div>

				        <ul class="contract_gallery">
					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>

					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>

					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <span class="gallery_item_remove"></span>
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>

					        <li>
						        <div class="contract_gallery_item">
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
					        <li>
						        <div class="contract_gallery_item">
							        <a data-fancybox="docs" href="build/images/contract_image.jpg">
								        <img src="build/images/contract_image.jpg" class="img-fluid" alt="">
							        </a>
							        <div class="contract_gallery_nav">
								        <select class="select_control">
									        <option value="" selected>Выберите категорию</option>
									        <option value="Категория 1">Категория 1</option>
									        <option value="Категория 2">Категория 2</option>
									        <option value="Категория 3">Категория 3</option>
								        </select>
							        </div>
						        </div>
					        </li>
				        </ul>
			        </div>

		        </div>

		        <aside class="contract_side">
			        <div class="contract_side_container">

				        <div class="contract_side_heading">
					        <span>Dokumente hochladen</span>
					        <a href="#">
						        <i>
							        <svg class="ico-svg"  viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
								        <use xlink:href="build/images/sprite_icons.svg#icon_cart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							        </svg>
						        </i>
					        </a>
				        </div>

				        <ul class="contract_side_total">
					        <li rel="total_valid" class="active"><span>Контракт</span></li>
					        <li rel="total_valid"><span>Паспорт</span></li>
					        <li rel="total_invalid"><span>Банковская карточка</span></li>
					        <li rel="total_other"><span>Прочее</span></li>
					        <li rel="total_check"><span>подпись клиента в договоре</span></li>
					        <li rel="total_check"><span>кОПИИ ВЕРНЫ</span></li>
				        </ul>

				        <div class="contract_side_wrap">

					        <button type="submit" class="btn_main btn_md btn_purple btn_checkout">
						        <span>weiter</span>
						        <i>
							        <svg class="ico-svg"  viewBox="0 0 12 22" fill="none" xmlns="http://www.w3.org/2000/svg">
								        <use xlink:href="build/images/sprite_icons.svg#icon_angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							        </svg>
						        </i>
					        </button>
				        </div>
			        </div>
		        </aside>

	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

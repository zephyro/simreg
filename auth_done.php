<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="auth_page">
            <div class="auth">
                <div class="auth_header">
                    <a href="#" class="logo">
                        <img src="build/images/logo.svg" class="img-fluid" alt="">
                        <span class="logo_line"></span>
                    </a>
                </div>
                <div class="auth_row">
                    <div class="auth_left">
                        <div class="auth_image">
                            <img src="build/images/auth_image.svg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="auth_right">
                        <div class="auth_form">
                            <div class="auth_form_title">Passwort Wiederherstellung</div>
                            <div class="auth_done">
                                <div class="auth_done_icon">OK</div>
                                <div class="auth_done_text">Ein Link zum Wiederherstellen Ihres Passworts wurde an Ihre E-Mail gesendet</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

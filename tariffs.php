<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">
		        </div>
		        <div class="account_heading_right">
			        <h1>РЕДАКТИРОВАНИЕ тарифа</h1>
		        </div>
	        </div>

	        <ul class="line_nav">
		        <li class="active"><a href="#">ПАКЕТЫ</a></li>
		        <li class=""><a href="#">Группы</a></li>
		        <li class=""><a href="#">СТАНДАРТНЫЕ ЦЕНЫ </a></li>
		        <li class=""><a href="#">Бонусы</a></li>
	        </ul>

	        <div class="content_box">
		        <div class="form_block mb-0">
			        <div class="form_block_title">
				        <div class="search_box">
					        <input type="text" class="form_control" name="search" placeholder="Search text">
				        </div>
				        <div class="search_result"></div>
			        </div>
			        <div class="form_block_content">
						<h3>Tariff</h3>

				        <ul class="rate">
					        <li class="disabled">
						        <div class="rate__title">Премия,€</div>
						        <div  class="rate__elem">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
							        </div>
							        <div class="rate__elem_legend">с 15/11/2018, 6€</div>
						        </div>
					        </li>
					        <li>
						        <div class="rate__title">Доп.бонус,€</div>
						        <a href="#" class="rate__elem">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
								        <div class="rate__elem_icon">
									        <i>
										        <img src="build/images/icon__bonus.svg" class="img-fluid" alt="">
									        </i>
								        </div>
							        </div>
							        <div class="rate__elem_legend">с 15/11/2018, 6€</div>
						        </a>
					        </li>
					        <li>
						        <div class="rate__text">AY ALLNET</div>
					        </li>
				        </ul>

				        <ul class="rate">
					        <li class="disabled">
						        <div class="rate__title">Премия,€</div>
						        <div  class="rate__elem">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
							        </div>
							        <div class="rate__elem_legend">с 15/11/2018, 6€</div>
						        </div>
					        </li>
					        <li>
						        <div class="rate__title">Доп.бонус,€</div>
						        <a href="#" class="rate__elem">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
								        <div class="rate__elem_icon">
									        <i>
										        <img src="build/images/icon__bonus.svg" class="img-fluid" alt="">
									        </i>
								        </div>
							        </div>
							        <div class="rate__elem_legend">с 15/11/2018, 6€</div>
						        </a>
					        </li>
					        <li>
						        <div class="rate__text">AY ALLNET</div>
					        </li>
				        </ul>

				        <div class="mb_30"></div>

				        <div class="form_block__heading">HW TARIFAFPREIS</div>

				        <ul class="rate">
					        <li class="disabled">
						        <div class="rate__title">Премия,€</div>
						        <div  class="rate__elem">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
							        </div>
							        <div class="rate__elem_legend">с 15/11/2018, 6€</div>
						        </div>
					        </li>
					        <li>
						        <div class="rate__title">Доп.бонус,€</div>
						        <a href="#" class="rate__elem">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
								        <div class="rate__elem_icon">
									        <i>
										        <img src="build/images/icon__bonus.svg" class="img-fluid" alt="">
									        </i>
								        </div>
							        </div>
							        <div class="rate__elem_legend">с 15/11/2018, 6€</div>
						        </a>
					        </li>
					        <li>
						        <div class="rate__text">AY ALLNET</div>
					        </li>
				        </ul>

				        <ul class="rate">
					        <li class="disabled">
						        <div class="rate__title">Премия,€</div>
						        <div  class="rate__elem">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
							        </div>
							        <div class="rate__elem_legend">с 15/11/2018, 6€</div>
						        </div>
					        </li>
					        <li>
						        <div class="rate__title">Доп.бонус,€</div>
						        <label class="rate__elem rate__elem_label">
							        <input type="hidden" value="" name="">
							        <div class="rate__elem_box">
								        <div class="rate__elem_value">
									        <span>10.99</span>
								        </div>
								        <div class="rate__elem_icon">
									        <i>
										        <img src="build/images/icon__bonus.svg" class="img-fluid" alt="">
									        </i>
								        </div>
							        </div>
						        </label>
					        </li>
					        <li>
						        <div class="rate__text">AY ALLNET Plus</div>
					        </li>
				        </ul>

				        <br/>

				        <button type="submit" class="btn_main btn_border">Сохранить</button>

			        </div>
		        </div>

	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

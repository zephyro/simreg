<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">
	        <div class="error">
		        <div class="error_wrap">
			        <div class="error_title">error</div>
			        <div class="error_box">
				        <div class="error_image">
					        <img src="build/images/error_image.svg" class="img-fluid" alt="">
				        </div>
				        <div class="error_text">Вы не заплатили за счет, обратитесь к Вашему оптовику, чтобы открыть доступ</div>
			        </div>
		        </div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

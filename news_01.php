<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">


    <!-- Header -->
    <?php include('inc/header_two.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">
			        <a href="#" class="btn_main">Добавить  новость</a>
		        </div>
		        <div class="account_heading_right">
			        <h1>Мои менеждеры</h1>
		        </div>
	        </div>

	        <div class="dataTable mt_30">
		        <ul class="views">
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="25" checked>
					        <span>25</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="50">
					        <span>50</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="100">
					        <span>100</span>
				        </label>
			        </li>
		        </ul>

		        <div class="table_responsive_md">
			        <div class="table_responsive">
				        <div class="table_top"></div>
				        <table class="table dataTable">

					        <tr class="table_head">
						        <th>#ID</th>
						        <th>Date</th>
						        <th>News</th>
						        <th>Ansicht</th>
					        </tr>

					        <tr class="table_nav">
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r2" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="DUNKLE LIESE">
												        <input type="radio" name="r2" value="DUNKLE LIESE">
												        <span>DUNKLE LIESE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="ESSER META">
												        <input type="radio" name="r2" value="ESSER META">
												        <span>ESSER META</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="HUMMEL MAYA">
												        <input type="radio" name="r2" value="HUMMEL MAYA">
												        <span>HUMMEL MAYA</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="SCHUSTER BARBARA">
												        <input type="radio" name="r2" value="SCHUSTER BARBARA">
												        <span>SCHUSTER BARBARA</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r2" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="DUNKLE LIESE">
												        <input type="radio" name="r2" value="DUNKLE LIESE">
												        <span>DUNKLE LIESE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="ESSER META">
												        <input type="radio" name="r2" value="ESSER META">
												        <span>ESSER META</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="HUMMEL MAYA">
												        <input type="radio" name="r2" value="HUMMEL MAYA">
												        <span>HUMMEL MAYA</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="SCHUSTER BARBARA">
												        <input type="radio" name="r2" value="SCHUSTER BARBARA">
												        <span>SCHUSTER BARBARA</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td>Alles</td>
						        <td></td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td>11/12/2018, 11:43</td>
						        <td>Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>



				        </table>
			        </div>
		        </div>

		        <div class="pagination">
			        <ul>
				        <li><a href="#">1</a></li>
				        <li><a href="#">2</a></li>
				        <li><span>3</span></li>
				        <li><a href="#">4</a></li>
				        <li><a href="#">5</a></li>
				        <li><a href="#">6</a></li>
			        </ul>
		        </div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">


    <!-- Header -->
    <?php include('inc/header_two.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">

		        </div>
		        <div class="account_heading_right">
			        <h1>добавление конфигурации</h1>
		        </div>
	        </div>

	        <div class="account">
		        <div class="account_side">
			        <div class="account_side_title">menu</div>

			        <ul class="account_menu">
				        <li class="active"><a href="#"><span>t-mobile</span></a></li>
				        <li><a href="#"><span>base</span></a></li>
				        <li><a href="#"><span>o2</span></a></li>
				        <li><a href="#"><span>vodafone</span></a></li>
			        </ul>

		        </div>
		        <div class="account_body">

			        <div class="account_top">
				        <div class="account_top_name">выберите конфигурацию</div>
				        <div class="account_top_actions"><a href="#">Удалить конфигурацию</a></div>
			        </div>

			        <div class="account_content">

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>Провайдер</h4>
					        </div>
					        <div class="account_box_content">
						        <div class="row">
							        <div class="col-xs-12 col-md-8 col-xl-6">
								        <div class="form_group">
									        <label class="form_label">Название провайдера</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control form_control_bold" value="T-MOBILE" placeholder="T-MOBILE">
									        </div>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>название конфигурации</h4>
					        </div>
					        <div class="account_box_content">
						        <div class="row">
							        <div class="col-xs-12 col-md-8 col-xl-6">
								        <div class="form_group">
									        <label class="form_label">Название конфигурации</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control form_control_bold" value="My konfiguration" placeholder="My konfiguration">
									        </div>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>настройки входа</h4>
					        </div>
					        <div class="account_box_content">
						        <div class="row">
							        <div class="col-xs-12 col-md-8 col-xl-6">
								        <div class="form_error_box">Не верный логин или пароль</div>
								        <div class="form_group">
									        <label class="form_label">ID dealer</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control form_control_bold" value="4385892390" placeholder="4385892390">
									        </div>
								        </div>
								        <div class="form_group">
									        <label class="form_label">Login</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control form_control_bold" value="Mylllogin" placeholder="Mylllogin">
									        </div>
								        </div>
								        <div class="form_group">
									        <label class="form_label">Password</label>
									        <div class="form_item">
										        <input type="text" name="" class="form_control form_control_bold" value="kjblsLKSJFKL" placeholder="kjblsLKSJFKL">
									        </div>
								        </div>
							        </div>
						        </div>
						        <div class="form_group">
							        <label class="form_checkbox">
								        <input type="checkbox" value="" name="">
								        <span>Магазин может менять пароль</span>
							        </label>
						        </div>
						        <div class="form_group">
							        <label class="form_checkbox">
								        <input type="checkbox" value="" name="">
								        <span>Возможность видеть конфигурацию и изменять пароль</span>
							        </label>
						        </div>
					        </div>
				        </div>

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>статус</h4>
					        </div>
					        <div class="account_box_content">
						        <div class="row">
							        <div class="col-xs-12 col-md-8 col-xl-6">
								        <div class="form_group">
									        <button class="btn_main btn_red_border">Заблокировать</button>
								        </div>
								        <div class="form_group">
									        <button class="btn_main">Разблокировать</button>
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>

				        <div class="account_box">
					        <div class="account_box_heading">
					        </div>
					        <div class="account_box_content no_border">
						        <button type="submit" class="btn_main btn_border btn_lg">Сохранить</button>
					        </div>
				        </div>


			        </div>
		        </div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

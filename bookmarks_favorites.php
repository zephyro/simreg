<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">
			        <div class="dropdown dropdown_tariff dropdown_right">
				        <a href="#" class="btn_main btn_sm dropdown_toggle">Создать закладку тарифа</a>
				        <div class="dropdown_menu">
					        <div class="dropdown_menu_wrap">
						        <ul>
							        <li>
								        <a href="#">
									        <b>
										        <img src="build/images/sim_logo.png" class="img-fluid" alt="">
									        </b>
									        <span>Au yildiz online</span>
								        </a>
							        </li>
							        <li>
								        <a href="#">
									        <b>
										        <img src="build/images/sim_logo.png" class="img-fluid" alt="">
									        </b>
									        <span>Au yildiz online</span>
								        </a>
							        </li>
						        </ul>
					        </div>
				        </div>
			        </div>

		        </div>
		        <div class="account_heading_right">
			        <h1>зАКЛАДКИ</h1>
		        </div>
	        </div>

	        <div class="dataTable mt_30">
		        <ul class="views">
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="25" checked>
					        <span>25</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="50">
					        <span>50</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="100">
					        <span>100</span>
				        </label>
			        </li>
		        </ul>

		        <div class="table_responsive_md">
			        <div class="table_responsive">
				        <div class="table_top"></div>
				        <table class="table dataTable">

					        <tr class="table_head">
						        <th>Название закладки</th>
						        <th>Где действует</th>
						        <th>вРЕМЯ ДЕЙСТВИЯ</th>
						        <th>Status</th>
						        <th>Ansicht</th>
					        </tr>

					        <tr class="table_nav">
						        <td></td>
						        <td></td>
						        <td></td>
						        <td></td>
						        <td></td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>


					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>


					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>-/-/</td>
						        <td><span class="text-purple">невидимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td><span class="text-strong">Ay Allnet plus 29€ + 5gb internt +500minut </span></td>
						        <td><span class="text-strong">вСЯ ГЕРМАНИЯ</span></td>
						        <td>05.03.2019-30.05.2019</td>
						        <td><span class="text-purple">видимая</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>


				        </table>
			        </div>
		        </div>
				<div class="pagination">
					<ul>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><span>3</span></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">6</a></li>
					</ul>
				</div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

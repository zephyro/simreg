<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

            <div class="provider">
                <div class="provider_title">Anbieter</div>
                <ul class="provider_row">
                   <li>
                       <label>
                           <input type="radio" name="provider" value="1" checked>
                           <div class="provider_item">
                               <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                               <span>Online</span>
                           </div>
                       </label>
                   </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                </ul>
            </div>

            <ul class="contract_progress">
                <li class="active"><span>1. Создание договора</span></li>
                <li><span>2. Подписание договора</span></li>
                <li><span>3. Краткая информация</span></li>
            </ul>

            <div class="block_warning block_error">
                <div class="block_warning_title"><span>Ошибка</span></div>
                <div class="block_warning_text">
                    <div class="block_warning_wrap">
                        Отклонен - ошибка технических данных
                        <br/>
                        Технические проблемы
                    </div>
                </div>
            </div>

            <div class="block_warning block_info">
                <div class="block_warning_title"><span>иНФО</span></div>
                <div class="block_warning_text">
                    <div class="block_warning_wrap">
                        <strong>Ожидаем договор от провайдера</strong>
                        <br/>
                        В ожидании договора
                    </div>
                </div>
            </div>

            <div class="block_warning block_info">
                <div class="block_warning_title center_block">

                    <div class="center_block__inner">
                        <div class="donut">
                            <div class="donut__chart">
                                <svg width="100%" height="100%" viewBox="0 0 42 42">
                                    <circle class="donut_hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                    <circle class="donut_ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#d2d3d4" stroke-width="3"></circle>
                                    <circle class="donut_segment" cx="21" cy="21" r="15.91549430918954" fill="transparent"  stroke-width="3" stroke-dasharray="60 40" stroke-dashoffset="0"></circle>
                                </svg>
                            </div>
                            <div class="donut__value"><div class="donut__value_inner">60%</div></div>
                        </div>
                    </div>

                </div>
                <div class="block_warning_text">
                    <div class="block_warning_wrap">
                        <strong>Ожидаем договор от провайдера</strong>
                        <br/>
                        В ожидании договора
                        <br/>
                        <strong>Ожидаем договор от провайдера</strong>
                        <br/>
                        В ожидании договора
                        <br/>
                        <strong>Ожидаем договор от провайдера</strong>
                        <br/>
                        В ожидании договора
                    </div>
                </div>
            </div>

            <ul class="contract_nav">
                <li class="active "><span>contract</span></li>
                <li><span>Info</span></li>
            </ul>

            <div class="contract">

                <div class="contract_content">

                    <div class="contract_data">

	                    <div class="contract_data_elem">
		                    <div class="contract_data_heading">
			                    <h4>Тариф</h4>

			                    <ul class="contract_list">
				                    <li>
					                    <label class="form_radio form_radio_md">
						                    <input type="radio" name="tariff_title" value="1" checked>
						                    <span>€14,99 Ay Allnet</span>
					                    </label>
				                    </li>
				                    <li>
					                    <label class="form_radio form_radio_md">
						                    <input type="radio" name="tariff_title" value="2">
						                    <span>€14,99 Ay Allnet Plus</span>
					                    </label>
				                    </li>
				                    <li>
					                    <label class="form_radio form_radio_md">
						                    <input type="radio" name="tariff_title" value="3">
						                    <span>€14,99 Ay Allnet Max</span>
					                    </label>
				                    </li>
			                    </ul>

		                    </div>

		                    <div class="contract_data_content">

			                    <div class="data_box">
				                    <ul class="checkbox_list text_uppercase">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_name" value="1" checked>
							                    <span>€14,99 Ay Allnet</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_name" value="2">
							                    <span>€14,99 Ay Allnet Plus</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_name" value="3">
							                    <span>€14,99 Ay Allnet Max</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">SPEZIAL RABATT</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_checkbox">
							                    <input type="radio" name="tariff_price_01" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> 100 % Rabatt auf Grundgebühr Türkei Allnet Option</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_checkbox">
							                    <input type="radio" name="tariff_price_01" value="2">
							                    <span><i class="data_profit profit_toggle">€0.00</i> 100 % Rabatt auf Grundgebühr Türkei Allnet Option</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">HW TARIFAUFPREIS</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_02" value="1" checked>
							                    <span><i class="data_profit color_orange profit_toggle">€0.00</i> Keine</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_02" value="2">
							                    <span><i class="data_profit color_orange profit_toggle">€2.00</i> HW-Band-Tarifaupreis 5 EUR</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_02" value="2">
							                    <span><i class="data_profit color_orange profit_toggle">€5.00</i> HW-Band-Tarifaupreis 10 EUR</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_02" value="2">
							                    <span><i class="data_profit color_orange profit_toggle">€6.00</i> HW-Band-Tarifaupreis 20 EUR</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">HW TARIFAUFPREIS</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_03" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> Keine</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_03" value="2">
							                    <span><i class="data_profit profit_toggle">€2.00</i> HW-Band-Tarifaupreis 5 EUR</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_03" value="2">
							                    <span><i class="data_profit profit_toggle">€5.00</i> HW-Band-Tarifaupreis 10 EUR</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_03" value="2">
							                    <span><i class="data_profit profit_toggle">€6.00</i> HW-Band-Tarifaupreis 20 EUR</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">INTERNET AY ALLNET MAX</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_04" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> Standard inklusive Internet Flat 16 GB</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_04" value="2">
							                    <span><i class="data_profit profit_toggle">€4.99</i> Internet Flat 20 GB</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">SMS PACKS</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_05" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> Standardabrechnung SMS</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">OPTIONAL SMS AND TELEFONIE PACKS</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_06" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> Türkei Allnet 60</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_06" value="2">
							                    <span><i class="data_profit profit_toggle">€4.99</i> 1000 SMS Allnet D/TR</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">ROAMING PACKS</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_07" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> Roaming Basic PayGo</span>
						                    </label>
					                    </li>
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_07" value="2">
							                    <span><i class="data_profit profit_toggle">€4.99</i> Tuerkei Internet 1,5GB</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">CONTENT UND SERVICES</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_05" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> Napster Music Flat</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

			                    <div class="data_box">
				                    <div class="data_box_title">CONTENT UND SERVICES</div>
				                    <ul class="checkbox_list">
					                    <li>
						                    <label class="form_radio">
							                    <input type="radio" name="tariff_price_05" value="1" checked>
							                    <span><i class="data_profit profit_toggle">€0.00</i> Napster Music Flat</span>
						                    </label>
					                    </li>
				                    </ul>
			                    </div>

		                    </div>
	                    </div>

                        <div class="contract_data_elem">
                            <div class="contract_data_heading">
                                <h4>Распознование</h4>
                            </div>
                            <div class="contract_data_content">
                                <div class="file_block">
                                    <div class="file_block_text">Система автоматического распознавания документов</div>
	                                <ul class="file_block_row">
		                                <li>
			                                <label class="file_upload">
				                                <div class="file_upload_wrap">
					                                <input type="file" name="" value="">
					                                <div class="file_upload_text">Перетащите файлы сюда или кликните для загрузки</div>
				                                </div>
			                                </label>
		                                </li>
		                                <li>
			                                <div class="qr">
				                                <div class="qr_code">
					                                <img src="build/images/qr.svg" class="img-fluid" alt="">
				                                </div>
			                                </div>
		                                </li>
	                                </ul>
                                </div>
                            </div>
                        </div>

                        <div class="contract_data_elem">
                            <div class="contract_data_heading">
                                <h4>dokument</h4>
                            </div>
                            <div class="contract_data_content">
                                <div class="form_group">
                                    <ul class="radio_group">
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="name1" value="1" checked disabled>
                                                <span>Personalausweis</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="name1" value="2" disabled>
                                                <span>Reisepass</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                                <label class="form_label">Staatsangehorigkeit</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <select class="form_select" disabled>
                                                <option value="Deutschland" selected>Deutschland</option>
                                                <option value="Deutschland">Deutschland</option>
                                                <option value="Deutschland">Deutschland</option>
                                                <option value="Deutschland">Deutschland</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" value="Ausweisnummer" placeholder="" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Gultig bis</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control form_date" value="03/05/1985" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contract_data_elem">
                            <div class="contract_data_heading">
                                <h4>imei</h4>
                            </div>
                            <div class="contract_data_content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label"></label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" value="imei hw-band-tarifaupreis" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contract_data_elem">
                            <div class="contract_data_heading">
                                <h4>sIM</h4>
                            </div>
                            <div class="contract_data_content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">simkarten nummer</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" value="123456789012"disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contract_data_elem">
                            <div class="contract_data_heading">
                                <h4>Käufer</h4>
                            </div>
                            <div class="contract_data_content">

                                <div class="form_group">
                                    <ul class="radio_group">
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="sex" value="1" checked disabled>
                                                <span>Herr</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="sex" value="2" disabled>
                                                <span>Frau</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Vorname</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" value="Alice" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Nachname</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" value="Alice" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Email</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" value="777@gmail.com" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Geburtsdatum</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control form_date" value="03/05/1985" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Рersönliches Kundenkennzahl</label>
                                            <div class="form_item form_item_error">
                                                <input type="text" name="" class="form_control" placeholder="" value="1651" disabled>
                                                <div class="form_item_info">Fehler: nur Zahlen</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">PLZ</label>
                                            <select class="form_select" disabled>
                                                <option value="Deutschland" selected>545845</option>
                                                <option value="Deutschland">545845</option>
                                                <option value="Deutschland">545845</option>
                                                <option value="Deutschland">545845</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">ORT</label>
                                            <select class="form_select" disabled>
                                                <option value="Deutschland" selected>Berlin</option>
                                                <option value="Deutschland">Berlin</option>
                                                <option value="Deutschland">Berlin</option>
                                                <option value="Deutschland">Berlin</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Straße</label>
                                            <select class="form_select" disabled>
                                                <option value="Deutschland" selected>BauAs Planstr.A Gewerbegebiet Drei</option>
                                                <option value="Deutschland">BauAs Planstr.A Gewerbegebiet Drei</option>
                                                <option value="Deutschland">BauAs Planstr.A Gewerbegebiet Drei</option>
                                                <option value="Deutschland">BauAs Planstr.A Gewerbegebiet Drei</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">HausNr</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" value="55" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contract_data_elem">
                            <div class="contract_data_heading">
                                <h4>Bank</h4>
                            </div>
                            <div class="contract_data_content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <ul class="radio_group">
                                                <li>
                                                    <label class="form_radio">
                                                        <input type="radio" name="bank" value="1">
                                                        <span>Kontonummer/BLZ</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_radio">
                                                        <input type="radio" name="bank" value="2" checked>
                                                        <span>IBAN/BIC</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label">IBAN</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" placeholder="" value="DE8583859348937924792" disabled>
                                            </div>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label">BIC</label>
                                            <div class="form_item">
                                                <input type="text" name="" class="form_control" placeholder="" value="DE8583859348937924792" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="bank_info">
                                            <div class="bank_info_name">Landesbank Berlin - Berliner Sparkasse</div>
                                            <ul>
                                                <li>
                                                    <strong>IBAN</strong>
                                                    <span>DE8583859348937924792</span>
                                                </li>
                                                <li>
                                                    <strong>BIC</strong>
                                                    <span>DE8583859348937924792</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button class="btn_main btn_md" type="submit">überprüfen</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contract_data_elem">
                            <div class="contract_data_heading">
                                <h4>Работник</h4>
                            </div>
                            <div class="contract_data_content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form_group">
                                            <label class="form_label">Выберите работника магазина</label>
                                            <select class="form_select" disabled>
                                                <option value="Deutschland" selected>Работник 1</option>
                                                <option value="Deutschland">Работник 2</option>
                                                <option value="Deutschland">Работник 3</option>
                                                <option value="Deutschland">Работник 4</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <aside class="contract_side">

	                <div class="contract_side_container">

		                <div class="contract_side_heading">
			                <span>Warenkorb</span>
			                <a href="#">
				                <i>
					                <svg class="ico-svg"  viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
						                <use xlink:href="build/images/sprite_icons.svg#icon_cart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
					                </svg>
				                </i>
			                </a>
		                </div>

		                <div class="contract_side_wrap">

			                <div class="contract_side_checkout">
				                <div class="contract_checkout_title"><span>Tarif</span></div>
				                <ul>
					                <li>
						                <div class="contract_checkout_value">€<span class="checkout_trice">29.99</span></div>
						                <div class="contract_checkout_legend">ay allnet plus</div>
					                </li>
				                </ul>
			                </div>

			                <div class="contract_side_checkout">
				                <div class="contract_checkout_title"><span>Packet</span></div>
				                <ul>
					                <li>
						                <div class="contract_checkout_value">€<span class="checkout_trice">5.00</span></div>
						                <div class="contract_checkout_legend">hw-band-tarifaupreis</div>
					                </li>
					                <li>
						                <div class="contract_checkout_value">€<span class="checkout_trice">0.00</span></div>
						                <div class="contract_checkout_legend">100% Rabatt auf Anschlusspreis-AyYildiz</div>
					                </li>
				                </ul>
			                </div>

			                <div class="contract_side_checkout">
				                <div class="contract_checkout_title"><span>insgesamt</span></div>
				                <ul>
					                <li>
						                <div class="contract_checkout_value">€<span class="checkout_trice">0.00</span></div>
						                <div class="contract_checkout_legend">Einmaling</div>
					                </li>
					                <li>
						                <div class="contract_checkout_value">€<span class="checkout_trice">59.59</span></div>
						                <div class="contract_checkout_legend">monatlich</div>
					                </li>
				                </ul>
			                </div>

			                <ul class="contract_side_summary">
				                <li>
					                <i>
						                <svg class="ico-svg"  viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							                <use xlink:href="build/images/sprite_icons.svg#icon_check" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
						                </svg>
					                </i>
					                <span>iMEI</span>
				                </li>
				                <li>
					                <i>
						                <svg class="ico-svg"  viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							                <use xlink:href="build/images/sprite_icons.svg#icon_check" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
						                </svg>
					                </i>
					                <span>sIM</span>
				                </li>
				                <li class="violet">
					                <i>
						                <svg class="ico-svg"  viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							                <use xlink:href="build/images/sprite_icons.svg#icon_check" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
						                </svg>
					                </i>
					                <span>kAUFER</span>
				                </li>
				                <li class="violet">
					                <i>
						                <svg class="ico-svg"  viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							                <use xlink:href="build/images/sprite_icons.svg#icon_check" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
						                </svg>
					                </i>
					                <span>dOKUMENT</span>
				                </li>
			                </ul>

			                <button type="submit" class="btn_main btn_md btn_purple btn_checkout">
				                <strong>€59.59</strong>
				                <span>weiter</span>
				                <i>
					                <svg class="ico-svg"  viewBox="0 0 12 22" fill="none" xmlns="http://www.w3.org/2000/svg">
						                <use xlink:href="build/images/sprite_icons.svg#icon_angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
					                </svg>
				                </i>
			                </button>
		                </div>

	                </div>

                </aside>
            </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

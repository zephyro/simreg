<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">
			        <a href="#" class="btn_main">Создание магазина</a>
		        </div>
		        <div class="account_heading_right">
			        <h1>meine Geschäfte</h1>
		        </div>
	        </div>

	        <div class="dataTable mt_30">
		        <ul class="views">
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="25" checked>
					        <span>25</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="50">
					        <span>50</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="100">
					        <span>100</span>
				        </label>
			        </li>
		        </ul>

		        <div class="table_responsive_md">
			        <div class="table_responsive">
				        <div class="table_top"></div>
				        <table class="table dataTable">

					        <tr class="table_head">
						        <th>#ID</th>
						        <th>Name</th>
						        <th class="dataTable_conf">Konfiguration</th>
						        <th>Premium</th>
						        <th>Status</th>
						        <th>Ansicht</th>
					        </tr>

					        <tr class="table_nav">
						        <td></td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r2" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="DUNKLE LIESE">
												        <input type="radio" name="r2" value="DUNKLE LIESE">
												        <span>DUNKLE LIESE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="ESSER META">
												        <input type="radio" name="r2" value="ESSER META">
												        <span>ESSER META</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="HUMMEL MAYA">
												        <input type="radio" name="r2" value="HUMMEL MAYA">
												        <span>HUMMEL MAYA</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="SCHUSTER BARBARA">
												        <input type="radio" name="r2" value="SCHUSTER BARBARA">
												        <span>SCHUSTER BARBARA</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td></td>
						        <td></td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r4" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r4" value="AKTIVE">
												        <span>AKTIVE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r4" value="	GESCHLOSSEN">
												        <span>	GESCHLOSSEN</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td></td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td>
							        partos.ayyildiz-BelikMobilfunkShop-21230763partos.ayyildiz-handel-UnitymediaShop-21227178partos.ayyildiz-handel-Akdeniz-21227156
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>262343</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa<br/>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2634234</td>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td><span class="color_red">Geschlossen</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">Hummel Maya</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">pUSHKIN ALEXEY</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td><span class="color_red">Geschlossen</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td>
							        <span>На рассмотрении</span>
							        <br/>
							        <a href="#" class="status_link">Проверить сейчас</a>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td>
							        partos.ayyildiz-BelikMobilfunkShop-21230763partos.ayyildiz-handel-UnitymediaShop-21227178partos.ayyildiz-handel-Akdeniz-21227156
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>262343</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa<br/>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2634234</td>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td><span class="color_red">Geschlossen</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">Hummel Maya</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">pUSHKIN ALEXEY</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td><span class="color_red">Geschlossen</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2352623</td>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td>
							        partos.ayyildiz-BelikMobilfunkShop-21230763partos.ayyildiz-handel-UnitymediaShop-21227178partos.ayyildiz-handel-Akdeniz-21227156
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>262343</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa<br/>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>2634234</td>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td><span class="color_red">Geschlossen</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">Hummel Maya</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">pUSHKIN ALEXEY</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td><span class="color_red">Geschlossen</span></td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td>23412</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        AY YILDIZ-minimal4837428-ttees m5924820<br/>
							        AY YILDIZ-minimal4837428-terrezalexxa
						        </td>
						        <td></td>
						        <td>AKTIVE</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

				        </table>
			        </div>
		        </div>
				<div class="pagination">
					<ul>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><span>3</span></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">6</a></li>
					</ul>
				</div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

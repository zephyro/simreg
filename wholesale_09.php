<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">


    <!-- Header -->
    <?php include('inc/header_two.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">

		        </div>
		        <div class="account_heading_right">
			        <h1>Alexiss rabotnikkkkk</h1>
		        </div>
	        </div>

	        <div class="account">
		        <div class="account_side">
			        <div class="account_side_title">menu</div>

			        <ul class="account_nav">
				        <li class="active">
					        <a href="#">
						        <span>Общие настройки</span>
						        <i class="account_nav_icon icon_setting">
							        <svg class="ico-svg"  viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg">
								        <use xlink:href="build/images/sprite_icons.svg#icon_setting" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							        </svg>
						        </i>
					        </a>
				        </li>
				        <li>
					        <a href="#">
						        <span>Мои настройки</span>
						        <i class="account_nav_icon icon_params">
							        <svg class="ico-svg"  viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
								        <use xlink:href="build/images/sprite_icons.svg#icon_params" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							        </svg>
						        </i>
					        </a>
				        </li>
				        <li>
					        <a href="#">
						        <span>выход</span>
						        <i class="account_nav_icon icon_logout">
							        <svg class="ico-svg" viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
								        <use xlink:href="build/images/sprite_icons.svg#icon_logout" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
							        </svg>
						        </i>
					        </a>
				        </li>
			        </ul>

		        </div>
		        <div class="account_body">

			        <div class="account_title">Общие настройки</div>

			        <div class="account_content">

				        <div class="account_box">
					        <div class="account_box_heading">
						        <h4>Мое лого</h4>
					        </div>
					        <div class="account_box_content">

						        <div class="form_group">
							        <div class="form_label">Загрузите Ваш логотип</div>
							        <label class="file_upload">
								        <div class="file_upload_wrap">
									        <input type="file" name="" value="">
									        <div class="file_upload_text">Перетащите файлы сюда или кликните для загрузки</div>
								        </div>
							        </label>
						        </div>

						        <div class="form_group">
							        <div class="account_logo">
								        <div class="account_logo_image">
									        <img src="build/images/logo.svg" class="img-fluid" alt="">
								        </div>
								        <a href="#" class="account_logo_remove">удалить <i class="fa fa-remove"></i></a>
							        </div>
						        </div>

					        </div>
				        </div>


				        <div class="account_box">
					        <div class="account_box_heading">
					        </div>
					        <div class="account_box_content no_border">
						        <button type="submit" class="btn_main btn_border btn_lg">Сохранить логотип</button>
					        </div>
				        </div>
			        </div>
		        </div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

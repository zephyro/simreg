<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">


    <!-- Header -->
    <?php include('inc/header_two.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">

		        </div>
		        <div class="account_heading_right">
			        <h1>создание нового менеждера</h1>
		        </div>
	        </div>

	        <div class="account_top">
		        <div class="account_top_name">Добавление магазина</div>
	        </div>

	        <div class="content_box">
		        <form class="form">

			        <div class="form_block">
				        <div class="form_block_title">данные входа</div>
				        <div class="form_block_content">

					        <div class="row">
						        <div class="col-xs-12 col-md-9 col-lg-7 col-xl-5">
							        <div class="form_group">
								        <label class="form_label">Фамилия и имя менеджера</label>
								        <div class="form_item">
									        <input type="text" name="" class="form_control form_control_bold" placeholder="Alex Pushkov">
								        </div>
							        </div>
							        <div class="form_group">
								        <label class="form_label">E-mail</label>
								        <div class="form_item">
									        <input type="text" name="" class="form_control form_control_bold" placeholder="info@gmail.com">
								        </div>
							        </div>
						        </div>
					        </div>
				        </div>
			        </div>

			        <div class="form_block">
				        <div class="form_block_title">Права пользователя</div>
				        <div class="form_block_content">

					        <div class="row">
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1" checked>
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1" checked>
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1" checked>
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
						        <div class="col-xs-12 col-md-6">
							        <div class="form_group">
								        <label class="form_checkbox">
									        <input type="checkbox" value="1">
									        <span>AY YILDIZ | Shop3943</span>
								        </label>
							        </div>
						        </div>
					        </div>

					        <br/>
					        <br/>

					        <button type="submit" class="btn_main btn_border btn_md">создать менеджера</button>

				        </div>
			        </div>


		        </form>

	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

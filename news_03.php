<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">
		        </div>
		        <div class="account_heading_right">
			        <h1>Добавление новости</h1>
		        </div>
	        </div>

	        <div class="account_top">
		        <div class="account_top_name">Новая новость</div>
	        </div>

	        <div class="content_box">
		        <form class="form">
			        <div class="form_block">
				        <div class="form_block_title">Текст новости</div>
				        <div class="form_block_content">
					        <div class="form_group">
						        <label class="form_label">Текст новости</label>
						        <div class="form_item">
							        <textarea class="form_control" name="" placeholder="" rows="3">Добро пожаловать в страну сказок, тут мы вам расскажем как создавалась история царицы прекрасной и илья муромца. Мы проведем вас по всей нашей истории и мы будем рады если Вы поддержите нас!</textarea>
						        </div>
					        </div>
				        </div>
			        </div>

			        <div class="form_block">
				        <div class="form_block_title">GEMEINSAM MIT</div>
				        <div class="form_block_content">
					        <div class="form_group">
						        <label class="form_label">Suchtext</label>
						        <div class="form_item">
							        <textarea class="form_control" name="" placeholder="" rows="2"></textarea>
						        </div>
					        </div>
					        <br/>
					        <button type="button" class="btn_main">Добавить новость</button>
				        </div>
			        </div>


		        </form>

	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

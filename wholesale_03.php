<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">


    <!-- Header -->
    <?php include('inc/header_two.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

	        <div class="account_heading">
		        <div class="account_heading_left">
			        <div class="dropdown">
				        <a href="#" class="btn_main btn_shadow">Добавить конфигурацию</a>
				        <div class="dropdown_menu dropdown_menu_arrow">
					        <div class="dropdown_menu_wrap">
						        <ul>
							        <li><a href="#">Провайдер 1</a></li>
							        <li><a href="#">Провайдер 2</a></li>
							        <li><a href="#">Провайдер 3</a></li>
						        </ul>
					        </div>
				        </div>
			        </div>
		        </div>
		        <div class="account_heading_right">
			        <h1>конфигурации для магазинов</h1>
		        </div>
	        </div>

	        <div class="dataTable mt_30">
		        <ul class="views">
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="25" checked>
					        <span>25</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="50">
					        <span>50</span>
				        </label>
			        </li>
			        <li>
				        <label>
					        <input type="radio" name="content_views" value="100">
					        <span>100</span>
				        </label>
			        </li>
		        </ul>

		        <div class="table_responsive_md">
			        <div class="table_responsive">
				        <div class="table_top"></div>
				        <table class="table dataTable">

					        <tr class="table_head">
						        <th>Name</th>
						        <th>Роль</th>
						        <th class="dataTable_conf">Konfiguration</th>
						        <th>Status</th>
						        <th>Ansicht</th>
					        </tr>

					        <tr class="table_nav">
						        <td></td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r2" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="DUNKLE LIESE">
												        <input type="radio" name="r2" value="DUNKLE LIESE">
												        <span>DUNKLE LIESE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="ESSER META">
												        <input type="radio" name="r2" value="ESSER META">
												        <span>ESSER META</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="HUMMEL MAYA">
												        <input type="radio" name="r2" value="HUMMEL MAYA">
												        <span>HUMMEL MAYA</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="SCHUSTER BARBARA">
												        <input type="radio" name="r2" value="SCHUSTER BARBARA">
												        <span>SCHUSTER BARBARA</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td></td>
						        <td>
							        <div class="sort_menu">
								        <div class="sort_menu_label" data-empty="Alles"><span>Alles</span></div>
								        <div class="sort_menu_dropdown">
									        <ul>
										        <li>
											        <label data-label="Alles">
												        <input type="radio" name="r4" value="Alles" checked>
												        <span>Alles</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r4" value="AKTIVE">
												        <span>AKTIVE</span>
											        </label>
										        </li>
										        <li>
											        <label data-label="">
												        <input type="radio" name="r4" value="	GESCHLOSSEN">
												        <span>	GESCHLOSSEN</span>
											        </label>
										        </li>
									        </ul>
								        </div>
							        </div>
						        </td>
						        <td></td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td class="dataTable_user">Особые настройки доступа</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Active</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Esser Meta</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Заключен</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        Конфигурация 1-3453805 293059205<br/>
							        Конфигурация 1-3453805 293059205
						        </td>
						        <td>
							        <a href="#" class="status_link">На рассмотрении</a>
							        <br/>
							        <a href="#" class="status_link">Проверить сейчас</a>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td class="dataTable_user">Особые настройки доступа</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Active</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Esser Meta</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Заключен</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        Конфигурация 1-3453805 293059205<br/>
							        Конфигурация 1-3453805 293059205
						        </td>
						        <td>
							        <a href="#" class="status_link">На рассмотрении</a>
							        <br/>
							        <a href="#" class="status_link">Проверить сейчас</a>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td class="dataTable_user">Особые настройки доступа</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Active</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Esser Meta</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Заключен</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        Конфигурация 1-3453805 293059205<br/>
							        Конфигурация 1-3453805 293059205
						        </td>
						        <td>
							        <a href="#" class="status_link">На рассмотрении</a>
							        <br/>
							        <a href="#" class="status_link">Проверить сейчас</a>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td class="dataTable_user">Особые настройки доступа</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Active</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Esser Meta</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Заключен</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        Конфигурация 1-3453805 293059205<br/>
							        Конфигурация 1-3453805 293059205
						        </td>
						        <td>
							        <a href="#" class="status_link">На рассмотрении</a>
							        <br/>
							        <a href="#" class="status_link">Проверить сейчас</a>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">aLEANIST DATEEE</td>
						        <td class="dataTable_user">Особые настройки доступа</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Active</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Esser Meta</td>
						        <td class="dataTable_user">Esser Meta</td>
						        <td>
							        Магазин 1, Магазин 2, Магазин 3, Магазин 4, Магазин 5, Магазин 6, Магазин 7
						        </td>
						        <td>Заключен</td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

					        <tr>
						        <td class="dataTable_user">Schuster Barbara</td>
						        <td class="dataTable_user">Schneider Martina</td>
						        <td>
							        Конфигурация 1-3453805 293059205<br/>
							        Конфигурация 1-3453805 293059205
						        </td>
						        <td>
							        <a href="#" class="status_link">На рассмотрении</a>
							        <br/>
							        <a href="#" class="status_link">Проверить сейчас</a>
						        </td>
						        <td>
							        <a href="#" class="dataTable_link">Ansicht</a>
						        </td>
					        </tr>

				        </table>
			        </div>
		        </div>

		        <div class="pagination">
			        <ul>
				        <li><a href="#">1</a></li>
				        <li><a href="#">2</a></li>
				        <li><span>3</span></li>
				        <li><a href="#">4</a></li>
				        <li><a href="#">5</a></li>
				        <li><a href="#">6</a></li>
			        </ul>
		        </div>
	        </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

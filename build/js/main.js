// SVG IE11 support
svg4everybody();

// --- Top Nav
$(function() {
    var pull = $('.nav_toggle');
    var menu = $('.page');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('nav_open');
    });
});

// Create templates for buttons
$.fancybox.defaults.btnTpl.rl = '<button data-fancybox-rl class="fancybox-button fancybox-button--rl" title="Rotate"><i class="fa fa-undo"></i></button>';
$.fancybox.defaults.btnTpl.rr = '<button data-fancybox-rr class="fancybox-button fancybox-button--rr" title="Rotate"><i class="fa fa-repeat"></i></button>';
$.fancybox.defaults.btnTpl.brightness = '<button data-fancybox-brightness class="fancybox-button fancybox-button--brightness" title="Brightness"><i class="fa fa-low-vision"></i></button>';
$.fancybox.defaults.btnTpl.inversion = '<button data-fancybox-inversion class="fancybox-button fancybox-button--inversion" title="Inversion"><i class="fa fa-file-text"></i></button>';
$.fancybox.defaults.btnTpl.circuit = '<button data-fancybox-circuit class="fancybox-button fancybox-button--circuit" title="Circuit"><i class="fa fa-object-ungroup"></i></button>';



$(function() {

    var degree = 0;


    $('body').on('click', '[data-fancybox-rl]', function() {

        var $elie = $(".fancybox-slide--current");

        console.log('rotate');
        degree = degree - 90;

        $elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)'});
        $elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)'});
        $elie.css({ '-ms-transform': 'rotate(' + degree + 'deg)'});
        $elie.css({ '-o-transform': 'rotate(' + degree + 'deg)'});
    });


    $('body').on('click', '[data-fancybox-rr]', function() {

        var $elie = $(".fancybox-slide--current");

        console.log('rotate');
        degree = degree + 90;

        $elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)'});
        $elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)'});
        $elie.css({ '-ms-transform': 'rotate(' + degree + 'deg)'});
        $elie.css({ '-o-transform': 'rotate(' + degree + 'deg)'});

    });

});



$('body').on('click', '[data-fancybox-brightness]', function() {
    console.log('brightness');
    $('.fancybox-slide--current').removeClass('inverted');
    $('.fancybox-slide--current').removeClass('sepia');

    $('.fancybox-slide--current').toggleClass('brightness');
});

$('body').on('click', '[data-fancybox-inversion]', function() {
    console.log('inversion');
    $('.fancybox-slide--current').removeClass('brightness');
    $('.fancybox-slide--current').removeClass('sepia');

    $('.fancybox-slide--current').toggleClass('inverted');
});

$('body').on('click', '[data-fancybox-circuit]', function() {

    $('.fancybox-slide--current').removeClass('inverted');
    $('.fancybox-slide--current').removeClass('brightness');


    $('.fancybox-slide--current').toggleClass('sepia');

});



$('[data-fancybox="docs"]').fancybox({
    buttons : [
        "circuit",
        "inversion",
        "brightness",
        "rl",
        "rr",
        "zoom",
        "fullScreen",
        "download",
        "thumbs",
        "close"
    ]
});


// Placeholders

$('.form_elem input').focus(function(event) {
    $(this).closest('.form_elem').addClass('focus');
});

$('.form_elem input').focusout(function(){

    var inputVal = $(this).closest('.form_elem').find('input').val();
    if (inputVal == '') {

    }

    $(this).closest('.form_elem').removeClass('focus');
});


// --- Form Select
$('.form_select').selectric({
    maxHeight: 250,
    disableOnMobile: true,
    nativeOnMobile: false,
    responsive: true
});

// --- Form Select
$('.select_control').selectric({
    maxHeight: 250,
    disableOnMobile: true,
    nativeOnMobile: false,
    responsive: true,
    customClass: {
        prefix: 'select'
    }
});


(function() {

    $('.contract_nav > li').on('click touchstart', function(e){
        e.preventDefault();

        if(!$(this).hasClass('active')) {
            $('.contract_nav').find('li').removeClass('active');
            $(this).addClass('active');
            $('.contract').find('.profit_toggle').toggleClass('visible');
        }
    });

}());


$('.info_block_remove').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.info_block').remove();
});

$('.dataTable_sort').on('click touchstart', function(e){
    e.preventDefault();
    $(this).toggleClass('sort_reverse');
});


$('.table_head_date span').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.table_head_status').find('span').removeClass('active');
    $(this).toggleClass('active');
});


$('.sort_menu_label').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.sort_menu').toggleClass('open');
});

$('.sort_menu_dropdown input[type="radio"]').change(function(e) {
    var box = $(this).closest('.sort_menu');
    box.removeClass('open');
    var txt = $(this).closest('label').attr("data-label");
  //  box.find('.sort_menu_label span').text(txt);
});

$('body').click(function (event) {

    if ($(event.target).closest(".sort_menu").length === 0) {
        $(".sort_menu").removeClass('open');
    }
});

$('.gallery_item_remove').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('li').remove();
});


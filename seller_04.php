<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

            <div class="provider">
                <div class="provider_title">Anbieter</div>
                <ul class="provider_row">
                   <li>
                       <label>
                           <input type="radio" name="provider" value="1" checked>
                           <div class="provider_item">
                               <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                               <span>Online</span>
                           </div>
                       </label>
                   </li>
                    <li>
                        <label>
                            <input type="radio" name="provider" value="1">
                            <div class="provider_item">
                                <i><img src="build/images/sim_logo.png" class="img-fluid" alt=""></i>
                                <span>Online</span>
                            </div>
                        </label>
                    </li>
                </ul>
            </div>

            <ul class="contract_progress">
                <li class="active"><span>1. Поиск договора</span></li>
                <li><span>2. Загрузка копий</span></li>
                <li><span>3. Краткая информация</span></li>
            </ul>

	        <div class="warning warning_info">
		        <div class="warning_wrap">
			        <strong>Создание оффлайн контракта</strong><br/>
			        Для поиска контракта заполните форму и нажмите “Search”
		        </div>
	        </div>

	        <div class="warning warning_wait">
		        <div class="warning_wrap">
			        Подождите, мы ищем...
		        </div>
	        </div>

	        <div class="warning warning_error">
		        <div class="warning_wrap">
			        Ошибка
		        </div>
	        </div>

	        <div class="warning warning_success">
		        <div class="warning_wrap">
			        Успешно, контакт найден
		        </div>
	        </div>

			<div class="content_box">
				<form class="form">
					<div class="form_wrap">
						<div class="form_block">
							<div class="form_block_title">Пользователь</div>
							<div class="form_block_content">
								<div class="form_group">
									<label class="form_label"><sup>*</sup>Data of birth</label>
									<div class="form_item">
										<input type="text" name="" class="form_control form_control_bold" placeholder="02.06.1975">
									</div>
								</div>
								<div class="form_group">
									<label class="form_label"><sup>*</sup>Peter hoffmaN</label>
									<div class="form_item">
										<input type="text" name="" class="form_control form_control_bold" placeholder="Peter hoffmaN" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="form_block">
							<div class="form_block_title">Пароль</div>
							<div class="form_block_content">
								<div class="form_group">
									<label class="form_label"><sup>*</sup>пароль</label>
									<div class="form_item">
										<input type="text" name="" class="form_control" placeholder="********">
									</div>
								</div>
								<div class="form_group">
									<label class="form_label"><sup>*</sup>Sim Cart</label>
									<div class="form_item">
										<input type="text" name="" class="form_control" placeholder="6516516516854684698" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form_block mb_10">
						<ul class="btn_group">
							<li>
								<button type="submit" class="btn_main btn_purple">Search</button>
							</li>
							<li>
								<button type="submit" class="btn_main">reset</button>
							</li>
							<li>
								<button type="submit" class="btn_main">cancel</button>
							</li>
						</ul>
					</div>
				</form>

			</div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

</body>

</html>

<header class="header">
    <div class="container">
        <div class="header_row">

            <nav class="header_nav">

                <a href="/" class="header_logo">
                    <img src="build/images/logo.svg" class="img-fluid" alt="">
                </a>

                <ul>
                    <li><a href="#"><span>Nachrichten</span></a></li>
                    <li><a href="#"><span>Kontrakt</span><b>5</b></a></li>
	                <li><a href="#"><span>Geschäfte</span></a></li>
	                <li><a href="#"><span>Manager</span></a></li>
	                <li><a href="#"><span>MEIN TEAM</span></a></li>
	                <li><a href="#"><span>Konfigurationen</span></a></li>
                </ul>

            </nav>

            <div class="header_content">

                <div class="dropdown dropdown_right">
                    <a href="#" class="header_sim dropdown_toggle">
                        <span>+ SIM-Karten</span>
                    </a>
	                <div class="dropdown_menu">
		                <div class="dropdown_menu_wrap">
			                <ul class="header_sim_dropdown">
				                <li>
					                <a href="#">
						                <b>
							                <img src="build/images/sim_logo.png" class="img-fluid" alt="">
						                </b>
						                <span>Au yildiz online</span>
					                </a>
				                </li>
				                <li>
					                <a href="#">
						                <b>
							                <img src="build/images/sim_logo.png" class="img-fluid" alt="">
						                </b>
						                <span>Au yildiz online</span>
					                </a>
				                </li>
			                </ul>
		                </div>
	                </div>
                </div>


                <a href="#" class="header_toggle nav_toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>

	            <div class="dropdown header_auth_dropdown dropdown_right">
                    <a href="#" class="header_auth dropdown_toggle">
                    <i>
                        <svg class="ico-svg"  viewBox="0 0 19 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="build/images/sprite_icons.svg#icon_user" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </a>
		            <div class="dropdown_menu">
			            <div class="dropdown_menu_wrap">
				            <ul class="header_auth_nav">
					            <li class="active">
						            <a href="#">
							            <i class="auth_user"></i>
							            <span>Peter Hoffmann</span>
						            </a>
					            </li>
					            <li>
						            <a href="#">
							            <i class="auth_params"></i>
							            <span>Общие настройки</span>
						            </a>
					            </li>
					            <li>
						            <a href="#">
							            <i class="auth_settings"></i>
							            <span>Мои настройки</span>
						            </a>
					            </li>
					            <li>
						            <a href="#">
							            <i class="auth_exit"></i>
							            <span>Выход</span>
						            </a>
					            </li>
				            </ul>
			            </div>
		            </div>
	            </div>

            </div>
        </div>
    </div>
</header>